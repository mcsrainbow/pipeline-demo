#!/bin/sh

# httpd
httpd

# tomcat
export TOMCAT_HOME=/opt/tomcat
/opt/tomcat/bin/startup.sh

# sshd
ssh-keygen -A
exec /usr/sbin/sshd -D -e "$@"
