#!/bin/bash

function update_jenkins_ip(){
  echo "1. Checking the IP address for Jenkins URL..."
  if $(grep -wq 'REALIP' data/jenkins/jenkins.model.JenkinsLocationConfiguration.xml);then
    echo "   Please input the IP address for Jenkins URL."
    echo "   It could be a private IP in LAN, or a public IP for Cloud instance."
    echo -n "   IP: "
    read ipaddr
    
    if [[ -z "${ipaddr}" ]]; then
      echo "ERROR: Incorrect IP"
      exit 1
    else
      sed -i s/REALIP/${ipaddr}/g data/jenkins/jenkins.model.JenkinsLocationConfiguration.xml
      grep jenkinsUrl data/jenkins/jenkins.model.JenkinsLocationConfiguration.xml
    fi
  fi
}

function check_depends(){
  echo "2. Checking the required softwares..."
  which docker-compose
  if [[ $? -ne 0 ]]; then
    echo "ERROR: No such docker-compose"
    exit 1
  fi
}

function ask_for_confirm(){
  echo "3. Please make sure the internet connection is stable and the download speed is good."
  echo -n "   [yes:no]: "
  read yesorno

  if [[ "${yesorno}" != "yes" ]]; then
    echo "ERROR: Expected 'yes' to continue"
    exit 1
  fi
}

function run_docker_compose(){
  echo "4. Running docker-compose up -d to start Jenkins, Stage and Prod containers..."
  docker-compose up -d
  if [[ $? -ne 0 ]]; then
    echo "ERROR: Exited"
    exit 1
  else
    docker-compose ps
  fi
}

function list_services(){
  echo "5. Here is the list of all services:"
  echo "   GitRepo: https://bitbucket.org/mcsrainbow/twdemo"
  echo ""
  echo "   Jenkins: http://${ipaddr}:9180"
  echo "     User/Password: admin/admin"
  echo ""
  echo "   Stage:"
  echo "     HTTPD: http://${ipaddr}:9280"
  echo "     TOMCAT: http://${ipaddr}:9281"
  echo "     SSH: 9222"
  echo "       User/Password: root/password"
  echo ""
  echo "   Prod:"
  echo "     HTTPD: http://${ipaddr}:9380"
  echo "     TOMCAT: http://${ipaddr}:9381"
  echo "     SSH: 9322"
  echo "       User/Password: root/password"
  echo ""
  echo "   PIPLINE:"
  echo "     Update codes on GitRepo - Trigger the job on Jenkins - Run the pipline:"
  echo "       Pull Codes - Build Codes(static.zip and ROOT.war) - Deploy to Stage(via Ansible) - Test Stage - Deploy to Prod(via Ansible) - Test Prod"

  exit 0
}

update_jenkins_ip
check_depends
ask_for_confirm
run_docker_compose
list_services
